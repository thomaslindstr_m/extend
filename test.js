// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var extend = require('./index.js');
    var assert = require('assert');

    describe('extend', function () {
        var parent; beforeEach(function () {
            parent = {
                obj: {
                    foo: 'bar',
                    bar: 'foo',
                    obj: {
                        foo: 'bar'
                    }
                },
                obj2: {
                    foo: 'bar',
                    bar: 'foo'
                },
                arr: [[0, {hello: 'there'}, 2], 1, 2, 3]
            };
        });

        it('should overwrite object keys', function () {
            var child = extend(parent, {
                obj2: 'hello'
            });

            assert.equal(child.obj2, 'hello');
        });

        it('unaffected object keys should persist', function () {
            var child = extend(parent, {
                obj: {
                    obj: {
                        bar: 'test'
                    }
                }
            });

            assert.equal(child.obj.obj.bar, 'test');
            assert.equal(child.obj.obj.foo, 'bar');
        });

        it('should be able to overwrite object keys in objects', function () {
            var child = extend(parent, {
                obj: {
                    foo: 'test',
                    obj: {
                        foo: 'foo'
                    }
                },
                arr: ['hello']
            });

            assert.equal(child.obj.foo, 'test');
            assert.equal(child.obj.obj.foo, 'foo');
            assert.equal(child.arr.length, 1);
            assert.equal(child.arr[0], 'hello');
        });

        it('should be able to set object keys in undefined objects', function () {
            var child = extend(parent, {
                obj3: {
                    foo: 'test',
                    foo3: {}
                }
            });

            assert.equal(child.obj3.foo, 'test');
        });

        it('should extend an empty object the same as one expects deep-copying to work', function () {
            var child = extend({}, parent);
            child.obj.foo = false;
            child.arr = ['hello'];

            assert.equal(parent.obj.foo, 'bar');
            assert.equal(child.obj.foo, false);
            assert.equal(parent.arr.length, 4);
            assert.equal(child.arr.length, 1);
        });

        it('should allow extending null while preserving parent', function () {
            var merge = extend(parent, null);
            assert.deepStrictEqual(merge, parent);
        });

        it('should replace an array when extending with an object', function () {
            var merge = extend({var:[]}, {var:{}});
            assert.deepStrictEqual(merge, {var:{}});
        });
    });
