// ---------------------------------------------------------------------------
//  extend
//
//  extends a parent object while letting keys from child overwrite it
//  - parent {Object}: Source object
//  - child {Object}: Object with keys to overwrite source object
// ---------------------------------------------------------------------------

    var objectHasProperty = require('@amphibian/object-has-property');
    var forOwn = require('@amphibian/for-own');
    var iterateUp = require('@amphibian/iterate-up');
    var isArray = require('@amphibian/is-array');
    var isObject = require('@amphibian/is-object');

    function extend(parent, child) {
        if (isObject(child)) {
            forOwn(child, function (key, value) {
                if (isObject(value)) {
                    if (objectHasProperty(parent, key)
                    && (isObject(parent[key]))) {
                        extend(parent[key], value);
                    } else {
                        parent[key] = extend({}, value);
                    }
                } else if (isArray(value)) {
                    parent[key] = extend([], value);
                } else {
                    parent[key] = value;
                }
            });
        } else if (isArray(child)) {
            iterateUp(child, function (index, i) {
                if (isObject(index)) {
                    if (objectHasProperty(parent, i)
                    && (isObject(parent[i]))) {
                        extend(parent[i], index);
                    } else {
                        parent[i] = extend({}, index);
                    }
                } else if (isArray(index)) {
                    parent[i] = extend([], index);
                } else {
                    parent[i] = index;
                }
            });
        } else if (child) {
            parent = child;
        }

        return parent;
    }

    module.exports = extend;
